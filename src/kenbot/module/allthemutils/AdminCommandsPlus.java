package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.registry.EventListener;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.listeners.CommandListener;
import org.pircbotx.Channel;
import org.pircbotx.Colors;
import org.pircbotx.User;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.MessageEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("UnusedDeclaration")
public class AdminCommandsPlus {
    private static Properties alias = new Properties();
    private static long startTime;

    public static void load() {
        File configFile = new File("data/", "aliases.db");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            alias.load(new FileInputStream(configFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Object o : alias.keySet()) {
            String command = (String) o;
            String target = alias.getProperty(command);
            Method method = CommandListener.getCommandMethod(target);
            if (method==null) {
                continue;
            }
            CommandListener.addCommand(command, method);
        }
    }

    public static void save() {
        File configFile = new File("data/", "aliases.db");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            alias.store(new FileOutputStream(configFile), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Command(help = "Announce message to all channels", usage = "announce <CHANNEL>", minArgs = 1, maxArgs = -1)
    public static void announce(MessageEvent event, String[] args) {
        String message = "$> From " + event.getUser().getNick() + ": " + OtherCommands.argsAsString(args);
        for (Channel channel : event.getBot().getChannels()) {
            channel.sendMessage(message);
        }
        event.getBot().sendNotice(event.getUser(), "Message Announced!");
    }

    @Command(help = "Greeting Manager", usage = "greeter <CHANNEL> (set,remove) [set<MESSAGE>,remove]", maxArgs = -1, minArgs = 2)
    public static void greeter(MessageEvent event, String[] args) {
        String channel = args[0];
        if (!channel.startsWith("#")) {
            messageUser(event.getUser().getNick(), "Error: First Argument is not a channel!");
            return;
        }
        String cmd = args[1];
        if (cmd.equalsIgnoreCase("set")) {
            if (args.length<2) {
                messageUser(event.getUser().getNick(), "Usage: greeter <CHANNEL> set <MESSAGE>");
                return;
            }
            String message = OtherCommands.argsAsString(args, 2);
            GreeterManager.channelGreetings.put(channel, message);
            GreeterManager.save();
            messageUser(event.getUser().getNick(), "Greeting set for " + channel);
        } else if (cmd.equalsIgnoreCase("remove")){
            GreeterManager.channelGreetings.remove(channel);
            messageUser(event.getUser().getNick(), "Removed Greeting for " + channel);
        } else if (cmd.equalsIgnoreCase("list")) {
            for (String greetChannel : GreeterManager.channelGreetings.keySet()) {
                String greeting = GreeterManager.channelGreetings.get(greetChannel);
                messageUser(event.getUser().getNick(), greetChannel + ": " + greeting);
            }
        }
    }

    @EventListener(event = "JoinEvent", name = "GreetOnJoin")
    public static void joinGreeter(JoinEvent event) {
        GreeterManager.load();
        String greeting = GreeterManager.getGreeting(event.getChannel());
        if (!greeting.equals("")) {
            event.getBot().sendNotice(event.getUser(), greeting);
        }
    }

    @Command(help = "Get the current amount of time the bot has been up.", usage = "uptime", maxArgs = 0, minArgs = 0)
    public static void uptime(MessageEvent event, String[] args) {
        long uptimeSeconds = TimeUnit.MILLISECONDS.toSeconds((System.currentTimeMillis() - startTime));
        event.getBot().sendNotice(event.getUser(), "Current Uptime: " + uptimeSeconds / 60 + " minutes");
    }

    @EventListener(event = "ConnectEvent", name = "UptimeCounter")
    public static void uptimeCount(ConnectEvent event) {
        startTime = System.currentTimeMillis();
    }

    @Command(help = "Colorify a string", usage = "colorify <STRING>", maxArgs = -1, minArgs = 1)
    public static void colorify(MessageEvent event, String[] args) {
        ColorRegistry.load();
        String out = ColorRegistry.replaceColor(OtherCommands.argsAsString(args));
        event.getChannel().sendMessage(out);
    }

    public static void messageUser(String name, String message) {
        BotUtils.getBot().sendNotice(name, message);
    }

    public static void messageUser(User name, String message) {
        BotUtils.getBot().sendNotice(name, message);
    }

    public static void messageUser(Channel name, String message) {
        BotUtils.getBot().sendNotice(name, message);
    }

    public static class ColorRegistry {

        protected static HashMap<String, String> colorMap = new HashMap<String, String>();

        public static void addColor(String name, String replacer) {
            colorMap.put(name, replacer);
        }

        public static String replaceColor(String string) {
            String[] strings = string.split(" ");
            for (int i = 0; i<strings.length; i++) {
                for (String colorName : colorMap.keySet()) {
                    if (strings[i].contains(colorName)) {
                        strings[i] = strings[i].replace(colorName, colorMap.get(colorName));
                    }
                }
            }
            return OtherCommands.argsAsString(strings);
        }

        public static void load() {
            addColor("blue", Colors.BLUE);
            addColor(("black"), Colors.BLACK);
            addColor("yellow", Colors.YELLOW);
            addColor("bold", Colors.BOLD);
            addColor("green", Colors.GREEN);
            addColor("cyan", Colors.CYAN);
            addColor("red", Colors.RED);
            addColor("teal", Colors.TEAL);
            addColor("underline", Colors.UNDERLINE);
            addColor("brown", Colors.BROWN);
            addColor("normal", Colors.NORMAL);
            addColor("reverse", Colors.REVERSE);
        }
    }

    @Command(help = "Add Command Alias", usage = "alias <COMMAND> <ALIAS>", maxArgs = 2, minArgs = 2)
    public static void alias(MessageEvent event, String[] args) {
        String command = args[1];
        String target = args[0];
        Method method = CommandListener.getCommandMethod(target);
        if (method==null) {
            event.getBot().sendNotice(event.getUser(), "No Such Command " + target);
            return;
        }
        CommandListener.addCommand(command, method);
        addAliasCmd(command, target);
        event.getBot().sendNotice(event.getUser(), "Added Command alias for " + target + " as " + command);
        save();
    }

    public static void addAliasCmd(String name, String target) {
        alias.setProperty(name, target);
    }
}
