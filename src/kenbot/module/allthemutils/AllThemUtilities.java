package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.IModule;
import kaendfinger.kenbot.api.configuration.Configuration;
import kaendfinger.kenbot.api.events.ConfigurationEvent;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class AllThemUtilities implements IModule {
    protected static boolean allowTextCommands;
    protected static boolean allowLogging;
    protected static boolean useNoticeForGoogle;
    protected static String owner;
    protected static String jiraAuth;
    protected static String bambooURL;
    protected static String[] servicesToHandle;
    protected static String bambooAuth;
    protected static boolean enableRawLog;
    protected static boolean enableDebugging;
    protected static String jiraURL;
    protected static int uptime;
    protected static String githubAuth;

    @Override
    public void load() {
        // Copy old text commmands config file if exists to new config file.
        try {
            File config = new File("data/text-commands.db");
            File privateConfig = new File("data/text-commands.private.db");
            File oldconfig = new File("textcommands.conf");
            File dataDir = new File("data/");
            if (!dataDir.exists()) {
                if (!dataDir.mkdirs()) {
                    System.out.println("ERROR: Cannot create data directory!");
                }
            }
            if (oldconfig.exists()) {
                oldconfig.renameTo(config);
            }
            if (!config.exists()) {
                config.createNewFile();
            }
            if (!privateConfig.exists()) {
                privateConfig.createNewFile();
            }
            TextCommands.loadProps();
            PrivateCmds.loadProps();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Get Load time
        Calendar rightNow = Calendar.getInstance();
        String time = rightNow.getTime().toString();
        // Load Text Commands log
        TextCmdsLog.load();
        // Load Greetings
        GreeterManager.load();
        // Load GitPolls
        GitPoller.PollDatabase.load();
        // Append the current load time to Text Commands log
        TextCmdsLog.append("<" + time + "> Loaded AllThemUtilities");
        File logFile = new File("log/raw.log");
        if (logFile.exists()) {
            logFile.delete();
        }
        RawLogger.init(new File("log/raw.log"));
        AdminCommandsPlus.load();
    }

    @Override
    public void unload() {
        TextCommands.storeProps();
        PrivateCmds.storeProps();
    }

    @Override
    public String[] classes() {
        return new String[] {
                "kenbot.module.allthemutils.AdminCommandsPlus",
                "kenbot.module.allthemutils.ExternalServices",
                "kenbot.module.allthemutils.FunnyCommands",
                "kenbot.module.allthemutils.GitPoller",
                "kenbot.module.allthemutils.OtherCommands",
                "kenbot.module.allthemutils.PrivateCmds",
                "kenbot.module.allthemutils.RawServiceHandler",
                "kenbot.module.allthemutils.TextCommands"

        };
    }

    @Override
    public String name() {
        return "AllThemUtilities";
    }

    @Override
    public void setup(ConfigurationEvent configEvent) {
        Configuration config = configEvent.getConfiguration();
        config.unlock();
        config.header("All Them Utilities Configuration");
        config.comment("Text Commands");
        config.comment("Can text commands be used in chat?");
        allowTextCommands = config.get("textcommands.enabled","true").equals("true");
        config.comment("Allow Module Logging?");
        allowLogging = configEvent.getConfiguration().get("allowLogging", "true").equals("true");
        config.comment("The login of the owner of the bot");
        String ownerFirst = config.get("owner", "");
        String jiraUsername = config.get("jiraUser", "changeme");
        String jiraPassword = config.get("jiraPass", "changeme");
        String bambooUsername = config.get("bambooUser", "changeme");
        String bambooPassword = config.get("bambooPass", "changeme");
        jiraAuth = jiraUsername + ":" + jiraPassword;
        bambooAuth = bambooUsername + ":" + bambooPassword;
        if (ownerFirst.equals("")) {
            owner = null;
        } else {
            owner = ownerFirst;
        }
        bambooURL = config.get("bambooURL", "https://directmyfile.atlassian.net/builds/");
        servicesToHandle = config.get("servicesToNoticeFor", "NickServ ChanServ BotServ MemoServ").split(" ");
        config.comment("Enable Logging every raw line? WARNING: Will create massive log and slight performance loss");
        enableRawLog = config.get("enableRawLog", "false").equals("true");
        enableDebugging = config.get("debug", "false").equals("true");
        jiraURL = config.get("jiraURL", "https://directmyfile.atlassian.net/");
        String githubUser = config.get("githubUser");
        String githubPass = config.get("githubPass");
        githubAuth = githubUser + " " + githubPass;
        if (githubAuth.equals(":")) {
            githubAuth = "";
        }
    }
}
