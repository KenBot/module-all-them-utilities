package kenbot.module.allthemutils;

@SuppressWarnings("all")
public class BambooAPI {
    class project {
        private results results;
        public project() {
            this.results = new results();
        }
        public results getResults() {
            return results;
        }
        class results {
            private result[] result;
            public results() {
                this.result = new result[]{};
            }
            public result[] getResults() {
                return result;
            }
        }
    }

    public plan createPlan() {
        return new plan();
    }

    public project createProject() {
        return new project();
    }

    class plan {
        private boolean isBuilding;
        private String name;

        public boolean isBuilding() { return isBuilding; }
        public String getName() { return  name; }
    }

    public build createBuild() {
        return new build();
    }

    class build {
        private String buildDurationDescription;
        private int buildDurationInSeconds;

        public String getBuildDurationDescription() {
            return buildDurationDescription;
        }

        public int getBuildDurationInSeconds() {
            return buildDurationInSeconds;
        }
    }

    class result {
        private String key;
        private String lifeCycleState;
        private String number;
        private String state;
        private artifacts artifacts;

        public String getState() {
            return state;
        }

        public String getKey() {
            return key;
        }

        public String getLifeCycleState() {
            return lifeCycleState;
        }

        public String getNumber() {
            return number;
        }

        public result() {
            this.artifacts = new artifacts();
        }

        public artifacts getArtifacts() {
            return artifacts;
        }

        class artifacts {
            private int size;
            private artifact[] artifact;

            public artifact[] getArtifacts() {
                return artifact;
            }
            public int getSize() {
                return size;
            }

            class artifact {
                private String name;
                private link link;
                private boolean shared;
                private int size;
                private String prettySizeDescription;

                public String getName() {
                    return name;
                }

                public link getLink() {
                    return link;
                }

                public int getSize() {
                    return size;
                }

                public boolean isShared() {
                    return shared;
                }

                public String getPrettySizeDescription() {
                    return prettySizeDescription;
                }

                class link {
                    private String href;

                    public String getURL() {
                        return href;
                    }
                }
            }
        }
    }
}