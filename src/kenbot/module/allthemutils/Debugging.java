package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.Command;
import org.pircbotx.hooks.events.MessageEvent;

@SuppressWarnings("unused")
public class Debugging {

    @Command
    public static void maxLineLength(MessageEvent event, String[] args) {
        if (!AllThemUtilities.enableDebugging) {
            return;
        }
        event.getBot().sendNotice(event.getUser(), Integer.toString(event.getBot().getMaxLineLength()));
    }

}
