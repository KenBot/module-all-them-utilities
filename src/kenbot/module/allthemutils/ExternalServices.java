package kenbot.module.allthemutils;

import com.google.gson.Gson;
import kaendfinger.kenbot.api.registry.Command;
import kenbot.module.allthemutils.api.HTTPUtils;
import org.pircbotx.hooks.events.MessageEvent;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class ExternalServices {

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Command(help = "Bamboo Notifications", usage = "bambooNotify <KEY>", maxArgs = 1, minArgs = 1)
    public static void bambooNotify(MessageEvent event, String[] args) {
        boolean isBuildRunning = false;
        boolean isBuilding;
        Gson gson = new Gson();
        String buildNumber = "";
        String planName;
        while (true) {
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String key = args[0];
            String urlString = AllThemUtilities.bambooURL + "rest/api/latest/plan/" + key + ".json?expand=artifacts";
            try {
                HttpURLConnection urlConnection = HTTPUtils.getConnection(urlString);
                if (urlConnection==null) {
                    event.getBot().sendNotice(event.getUser(), "Error: Could not get the URL Connection.");
                    return;
                }
                BASE64Encoder enc = new sun.misc.BASE64Encoder();
                String userpassword = AllThemUtilities.bambooAuth;
                String encodedAuthorization = enc.encode(userpassword.getBytes());
                urlConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
                urlConnection.connect();
                if (urlConnection.getResponseCode()!=200) {
                    continue;
                }
                InputStreamReader inputStreamReader = new InputStreamReader(urlConnection.getInputStream());
                BambooAPI bambooJSON = new BambooAPI();
                BambooAPI.plan plan = bambooJSON.createPlan();
                BambooAPI.plan newPlan = gson.fromJson(inputStreamReader, plan.getClass());
                isBuilding = newPlan.isBuilding();
                planName = newPlan.getName();
                if (isBuilding) {
                    if (isBuildRunning) {
                        continue;
                    }
                    ArrayList<String> list = new ArrayList<>();
                    list.add("true");
                    Map<String, Object> buildInfo = getBambooBuildInfo(key, urlString, false);
                    if (buildInfo==null) {
                        event.getBot().sendNotice(event.getUser(), "Error! Could not get BuildInfo!");
                    }
                    buildNumber = (String) buildInfo.get("number");
                    event.getChannel().sendMessage("$> Bamboo: " + key + " (" + planName + ") #" + buildNumber + " starting....");
                    isBuildRunning = true;
                } else {
                    if (isBuildRunning) {
                        Map<String, Object> buildInfo = getBambooBuildInfo(key, urlString, true);
                        String state = (String) buildInfo.get("state");
                        String buildDuration = (String) buildInfo.get("buildDurationDescription");
                        ArrayList<String> messages = new ArrayList<>();
                        messages.add("$> Bamboo: " + state + "! " + key + " (" + planName + ") #" + buildNumber + " completed (Taking " + buildDuration + ")");
                        for (String message : messages) {
                            event.getChannel().sendMessage(message);
                        }
                        isBuildRunning = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map<String, Object> getBambooBuildInfo(String key, String urlString, boolean isCompleted) throws IOException {
        Gson gson = new Gson();
        String bidURL = urlString.replace("/plan", "/result");
        BambooAPI bambooJSON = new BambooAPI();
        BASE64Encoder enc = new sun.misc.BASE64Encoder();
        String userpassword = AllThemUtilities.jiraAuth;
        String encodedAuthorization = enc.encode(userpassword.getBytes());
        HttpURLConnection bidurlConnection = HTTPUtils.getConnection(bidURL);
        if (bidurlConnection==null) {
            return null;
        }
        bidurlConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
        bidurlConnection.connect();
        InputStreamReader bidinputReader = new InputStreamReader(bidurlConnection.getInputStream());
        BambooAPI.project project = bambooJSON.createProject();
        BambooAPI.project newProject = gson.fromJson(bidinputReader, project.getClass());
        Map<String, Object> keys = new HashMap<>();
        keys.put("number", newProject.getResults().getResults()[0].getNumber());
        keys.put("state", newProject.getResults().getResults()[0].getState());
        // Get Other Build Info
        if (isCompleted) {
            String bidURLBuild = urlString.replace("plan/" + key + ".json", "result/" + key + "-" + keys.get("number") + ".json");
            HttpURLConnection bidurlConnectionBuild = HTTPUtils.getConnection(bidURLBuild);
            if (bidurlConnectionBuild==null) {
                return null;
            }
            bidurlConnectionBuild.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
            bidurlConnectionBuild.connect();
            if (bidurlConnectionBuild.getResponseCode()!=200) {
                System.out.println("Error!");
                return keys;
            }
            InputStreamReader bidinputReaderBuild = new InputStreamReader(bidurlConnection.getInputStream());
            BambooAPI.build build = bambooJSON.createBuild();
            BambooAPI.build newBuild = gson.fromJson(bidinputReaderBuild, build.getClass());
            keys.put("buildDurationDescription", newBuild.getBuildDurationDescription());
        }
        return keys;
    }
}