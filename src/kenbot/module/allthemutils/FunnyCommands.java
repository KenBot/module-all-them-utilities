package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.utils.BufferUtils;
import kaendfinger.kenbot.api.utils.StringUtils;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class FunnyCommands {
    @Command(help = "Check if person is five years old.", usage = "isfive <TARGET>", maxArgs = 1, minArgs = 1)
    public static void isfive(MessageEvent event, String[] args) {
        event.getChannel().sendMessage("$> Yes, indeed " + args[0] + " is always five years old.");
    }

    @Command(help = "Makes...", usage = "make <SOMETHING>", maxArgs = -1, minArgs = 1)
    public static void make(MessageEvent event, String[] args) {
        StringBuilder build = new StringBuilder();
        for (int i = 0 ; i<args.length ; i++) {
            if (args[i].equals("me")) {
                args[i] = "you";
            }
            if (i==args.length - 1) {
                build.append(args[i]);
            } else {
                build.append(args[i]).append(" ");
            }
        }
        event.getChannel().sendMessage("$> I am now making " +  build.toString());
    }
}
