package kenbot.module.allthemutils;

public class GitHubCommits {
    private author author;
    private commit commit;

    public GitHubCommits() {
        this.author = new author();
        this.commit = new commit();
    }

    public author getAuthor() {
        return author;
    }

    public commit getCommit() {
        return commit;
    }

    @SuppressWarnings("UnusedDeclaration")
    class commit{
        private String message;
        private tree tree;

        public String getMessage() {
            return message;
        }

        public commit() {
            this.tree =  new tree();
        }

        public tree getTree() {
            return tree;
        }

        class tree {
            private String sha;

            public String getSha() {
                return sha;
            }

        }
    }

    @SuppressWarnings("UnusedDeclaration")
    class author {
        private String login;

        public String getLogin() {
            return this.login;
        }

        public void setLogin(String login) {
            this.login = login;
        }
    }
}
