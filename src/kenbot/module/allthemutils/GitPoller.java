package kenbot.module.allthemutils;

import com.google.gson.Gson;
import kaendfinger.kenbot.api.Log;
import kaendfinger.kenbot.api.registry.Command;
import kenbot.module.allthemutils.api.HTTPUtils;
import org.pircbotx.hooks.events.MessageEvent;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.HttpURLConnection;
import java.util.Properties;

public class GitPoller {
    @Command(help = "Start Polling for GitHub", usage = "githubpoll USER REPO", minArgs = 2, maxArgs = 2)
    public static void githubpoll(MessageEvent event, String[] args) throws IOException {
        String sha = "";
        boolean shouldContinue = true;
        String lastMod = "";
        HttpURLConnection httpURLConnection = null;
        if (PollDatabase.exists("github/"  + args[0])) {
            sha = PollDatabase.get("github/" + args[0]);
        }
        try {
            String encodedAuthorization = "";
            if (!AllThemUtilities.githubAuth.equals("")) {
                BASE64Encoder enc = new sun.misc.BASE64Encoder();
                String userpassword = AllThemUtilities.githubAuth;
                encodedAuthorization = enc.encode(userpassword.getBytes());
            }
            int tries = 0;
            while (shouldContinue) {
                httpURLConnection = HTTPUtils.getConnection("https://api.github.com/repos/" + args[0] + "/" + args[1] + "/commits");
                if (httpURLConnection==null) {
                    tries++;
                    if (tries==5) {
                        event.getBot().sendNotice(event.getUser(), "Error! Could not get connection to GitHub API.");
                        return;
                    }
                    continue;
                }
                if (!encodedAuthorization.equals("")) {
                    httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
                }
                if (!lastMod.equals("")) {
                    httpURLConnection.setRequestProperty("If-Modified-Since", lastMod);
                }
                httpURLConnection.connect();
                if (httpURLConnection.getResponseCode()==304) {
                    Thread.sleep(1500);
                    continue;
                } else if (httpURLConnection.getResponseCode()==404) {
                    event.getBot().sendNotice(event.getUser(), "Error! Repository does not exist!");
                    break;
                } else if (httpURLConnection.getResponseCode()!=200) {
                    event.getBot().sendNotice(event.getUser(), "Git Poller: Unknown error.");
                    break;
                }
                lastMod = httpURLConnection.getHeaderField("Last-Modified");
                Gson gson = new Gson();
                GitHubCommits[] commits = gson.fromJson(new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream())), GitHubCommits[].class);
                String sha1a = commits[0].getCommit().getTree().getSha();
                if (sha1a.equals(sha)) {
                    Thread.sleep(1500);
                    continue;
                }
                Log.console("Found Commit for " + args[0] + "/" + args[1]);
                String author = commits[0].getAuthor().getLogin();
                String commitMessage = commits[0].getCommit().getMessage();
                sha = sha1a;
                event.getChannel().sendMessage("$> New Commit on " + args[0] + "/" + args[1] + ": \"" + commitMessage + "\"" + " by " + author);
                PollDatabase.set("github/" + args[0], sha1a);
                PollDatabase.save();
            }
        } catch (Exception e) {
            e.printStackTrace();
            assert httpURLConnection != null;
            httpURLConnection.disconnect();
        }
        httpURLConnection.disconnect();
    }

    static class PollDatabase {
        private static Properties prop = new Properties();

        public static void load() {
            try {
                File propFile = new File("data/polling.db");
                if (!propFile.exists()) {
                    if (!propFile.createNewFile()) {
                        return;
                    }
                    prop.store(new OutputStreamWriter(new FileOutputStream("data/polling.db")), null);
                } else {
                    prop.load(new InputStreamReader(new FileInputStream("data/polling.db")));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static String get(String id) {
            return prop.getProperty(id);
        }

        public static void set(String id, String commit) {
            prop.setProperty(id, commit);
        }

        public static boolean exists(String id) {
            return prop.containsKey(id);
        }

        public static void save() {
            try {
                prop.store(new OutputStreamWriter(new FileOutputStream("data/polling.db")), null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}