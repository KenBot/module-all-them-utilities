package kenbot.module.allthemutils;

import java.util.List;

public class GoogleResults {

    private ResponseData responseData;

    public GoogleResults(ResponseData responseData) {
        this.responseData = responseData;
    }

    public ResponseData getResponseData() { return responseData; }
    public String toString() { return "ResponseData[" + responseData + "]"; }

    static class ResponseData {
        private List<Result> results;

        ResponseData(List<Result> results) {
            this.results = results;
        }

        public List<Result> getResults() { return results; }
        public String toString() { return "Results[" + results + "]"; }
    }

    static class Result {
        private String url;
        private String titleNoFormatting;

        Result(String url, String titleNoFormatting) {
            this.url = url;
            this.titleNoFormatting = titleNoFormatting;
        }

        public String getUrl() { return url; }
        public String getTitle() { return titleNoFormatting; }
        public String toString() { return "Result[url:" + url +",titleNoFormatting:" + titleNoFormatting + "]"; }
    }

}