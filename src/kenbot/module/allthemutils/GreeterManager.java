package kenbot.module.allthemutils;

import org.pircbotx.Channel;

import java.io.*;
import java.util.HashMap;

public class GreeterManager implements Serializable {
    public static HashMap<String, String> channelGreetings = new HashMap<String, String>();

    public static void save() {
        File file = new File("data/ChannelGreetings.dat");
        try {
            FileOutputStream out = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(channelGreetings);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @SuppressWarnings("unchecked")
    public static void load() {
        File file = new File("data/ChannelGreetings.dat");
        if (!file.exists()) {
            return;
        }
        try {
            FileInputStream in = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(in);
            Object readObject = is.readObject();
            if (readObject instanceof HashMap) channelGreetings = (HashMap<String, String>) readObject; else System.out.println("Error! Could not load Channel Greetings.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getGreeting(Channel channel) {
        if (channelGreetings.containsKey(channel.getName())) {
            return "[" + channel.getName() + "] " + channelGreetings.get(channel.getName());
        } else {
            return "";
        }
    }
}
