package kenbot.module.allthemutils;

public class JiraAPI {
    @SuppressWarnings("UnusedDeclaration")
    class JiraIssue {
        private fields fields;
        private String errorMessages;
        private String key;

        public JiraIssue() {
            this.fields = new fields();
        }

        public void setKey(String key) {
            this.key = key;
        }

        public void setErrorMessages(String errorMessages)  {
            this.errorMessages = errorMessages;
        }

        public String getKey() {
            return key;
        }
        public fields getFields() {
            return fields;
        }

        class fields {
            private String summary;
            private reporter reporter;
            private status status;
            private assignee assignee;

            public fields() {
                this.reporter = new reporter();
                this.status = new status();
                this.assignee = new assignee();
            }

            public void setSummary(String summary) {
                this.summary = summary;
            }

            public String getSummary() {
                return summary;
            }

            public status getStatus() {
                return status;
            }

            public reporter getReporter() {
                return this.reporter;
            }

            public assignee getAssignee() {
                return assignee;
            }

            class assignee {
                private String name;

                public assignee() {

                }

                public String getName() {
                    return this.name;
                }
                public void setName(String par1) {
                    this.name = par1;
                }
            }

            class reporter {
                private String displayName;

                public reporter() {

                }

                public String getDisplayName() {
                    return displayName;
                }

                public void setDisplayName(String name) {
                    this.displayName = name;
                }
            }

            class status {
                private String name;

                public String getName() {
                    return name;
                }

                public void setName(String par1) {
                    this.name = par1;
                }
            }

        }
    }
}
