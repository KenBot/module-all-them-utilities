package kenbot.module.allthemutils;

import com.google.gson.Gson;
import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.registry.EventListener;
import kaendfinger.kenbot.api.utils.StringUtils;
import kenbot.module.allthemutils.api.HTTPUtils;
import kenbot.module.allthemutils.core.RandomEntities;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NickChangeEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sun.misc.BASE64Encoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OtherCommands {
    public static Map<Channel, String> channelsAutoVoicing = new HashMap<Channel, String>();
    public static Map<String, String> nickChanges = new HashMap<String, String>();

    @Command(help = "Search Google", usage = "google <query>", maxArgs = -1, minArgs = 1)
    public static void google(MessageEvent event, String[] args) {
        String queryString = argsAsString(args);
        String google = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
        String charset = "UTF-8";
        URL url = null;
        try {
            url = new URL(String.format("%s%s", google, URLEncoder.encode(queryString, charset)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            event.getBot().sendNotice(event.getUser(), "Sorry! An error occured");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assert url != null;
        Reader reader = null;
        try {
            reader = new InputStreamReader(url.openStream(), charset);
        } catch (IOException e) {
            e.printStackTrace();
        }
        GoogleResults results = new Gson().fromJson(reader, GoogleResults.class);
        String pageURL = results.getResponseData().getResults().get(0).getUrl();
        try {
            pageURL = URLDecoder.decode(pageURL, charset);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String pageTitle = results.getResponseData().getResults().get(0).getTitle();
        String out = "$> " + pageTitle + " | " + pageURL;
        if (!AllThemUtilities.useNoticeForGoogle) {
            event.getChannel().sendMessage(out);
        } else {
            event.getBot().sendNotice(event.getUser(), out);
        }
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @EventListener(name = "NickChangeLogger", event = "NickChangeEvent")
    public static void nickChangePutter(NickChangeEvent event) {
        nickChanges.put(event.getOldNick(), event.getNewNick());
    }
    @Command(help = "Get a URL's page title", usage = "pagetitle <URL>", minArgs = 1, maxArgs = 1)
    public static void pagetitle(MessageEvent event, String[] args) throws Exception {
        String url = args[0];
        event.getBot().sendNotice(event.getUser(), TitleExtractor.getPageTitle(url));
    }

    @Command(help = "Send a RAW line.", usage = "raw <LINE>", maxArgs = -1, minArgs = 1)
    public static void raw(MessageEvent event, String[] args) {
        String line = argsAsString(args);
        event.getBot().sendRawLineNow(line);
    }

    @Command(help = "Start Auto-Voicing", usage = "autovoice")
    public static void autovoice(MessageEvent event) {
        if (channelsAutoVoicing.containsKey(event.getChannel())) {
            event.getBot().sendNotice(event.getUser(), "This channel is already auto-voicing! Use the command \"autovoiceoff\" to turn auto-voicing off.");
            return;
        }
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "Sorry. You are not allowed to start AutoVoicing!");
            return;
        }
        event.getBot().sendNotice(event.getChannel(), "This channel will now Auto-Voice users who join!");
        channelsAutoVoicing.put(event.getChannel(), "true");

    }

    @Command(help = "Stop Auto-Voicer", usage = "stopautovoice")
    public static void stopautovoice(MessageEvent event) {
        if (channelsAutoVoicing.containsKey(event.getChannel())) {
            channelsAutoVoicing.put(event.getChannel(), "false");
        } else {
            event.getBot().sendNotice(event.getUser(), "This channel is not auto-voicing!");
        }
    }

    @Command(help = "Message a person or channel", usage = "msg <TARGET> <MESSAGE>", minArgs = 2, maxArgs = -1)
    public static void botmsg(MessageEvent event, String[] args) {
        event.getBot().sendMessage(args[0], StringUtils.getArgsAsString(args, 1));
    }

    @Command(help = "Message NickServ with arguments", usage = "nickserv <MSG>", minArgs = 1, maxArgs = -1)
    public static void nickserv(MessageEvent event, String[] args) {
        String nargs = argsAsString(args);
        event.getBot().sendMessage("NickServ", nargs);
    }

    @Command(help = "Ban then Kick a user", usage = "kickban <TARGET> [REASON]", maxArgs = -1, minArgs = 1)
    public static void kickban(MessageEvent event, String[] args) {
        if (args.length>1) {
            String reason = argsAsString(args, 1);
            if (event.getChannel().isOp(event.getBot().getUserBot())) {
                event.getBot().ban(event.getChannel(), args[0]);
                if (!event.getBot().getUser(args[0]).getChannels().contains(event.getChannel())) {
                    return;
                }
                event.getBot().kick(event.getChannel(), event.getBot().getUser(args[0]), reason);
                return;
            } else {
                event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
                return;
            }
        }
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().ban(event.getChannel(), args[0]);
            event.getBot().kick(event.getChannel(), event.getBot().getUser(args[0]), event.getUser().getNick() + " said to");
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Ban a user", usage = "ban <TARGET>", maxArgs = 1, minArgs = 1)
    public static void ban(MessageEvent event, String[] args) {
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().ban(event.getChannel(), args[0]);
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Un-Ban a user", usage = "unban <TARGET>", maxArgs = 1, minArgs = 1)
    public static void unban(MessageEvent event, String[] args) {
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().unBan(event.getChannel(), args[0]);
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Kick a user", usage = "kick <TARGET>", maxArgs = 1, minArgs = 1)
    public static void kick(MessageEvent event, String[] args) {
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().kick(event.getChannel(), event.getBot().getUser(args[0]));
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Quiet a user", usage = "quiet <TARGET>", maxArgs = 1, minArgs = 1)
    public static void quiet(MessageEvent event, String[] args) {
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().setMode(event.getChannel(), "+q", args[0]);
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Unquiet a user", usage = "unquiet <TARGET>", maxArgs = 1, minArgs = 1)
    public static void unquiet(MessageEvent event, String[] args) {
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().setMode(event.getChannel(), "-q", args[0]);
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Sets Topic", usage = "topic <MESSAGE>", maxArgs = -1, minArgs = 1)
    public static void topic(MessageEvent event, String[] args) {
        String topicSet = StringUtils.arrayAsString(args);
        if (event.getChannel().isOp(event.getBot().getUserBot())) {
            event.getBot().setTopic(event.getChannel(), topicSet);
            event.getBot().sendNotice(event.getUser(), "Topic Set!");
        } else {
            event.getBot().sendNotice(event.getUser(), "Bot does not have permission!");
        }
    }

    @Command(help = "Toggle topic protection", usage = "topicprotect", minArgs = 0, maxArgs = 0)
    public static void topicprotect(MessageEvent event, String[] args) {
        if (event.getChannel().hasTopicProtection()) {
            if (event.getBot().userExists("ChanServ"))
            event.getBot().sendMessage("ChanServ", "set TOPICPROTECT " + event.getChannel().getName() + " on");
        } else {
            event.getBot().setTopicProtection(event.getChannel());
        }
    }

    @Command(help = "Toggle Invite Only", usage = "inviteonly", maxArgs = 0, minArgs = 0)
    public static void inviteonly(MessageEvent event, String[] args) {
        if (event.getChannel().isInviteOnly()) {
            event.getBot().removeInviteOnly(event.getChannel());
        } else {
            event.getBot().setInviteOnly(event.getChannel());
        }
    }

    @Command(help = "Send a Memo", usage = "sendmemo <TARGET> <TEXT>", maxArgs = -1, minArgs = 2)
    public static void sendmemo(MessageEvent event, String[] args) {
        String target = args[0];
        String message = argsAsString(args, 1);
        event.getBot().sendMessage(event.getBot().getUser("ChanServ"), "send " + target + " From " + event.getUser().getNick() + ": " + message);
        event.getBot().sendNotice(event.getUser(), "Message Sent!");
    }

    @Command(help = "List Nick Changes", usage = "listnickchanges")
    public static void listnickchanges(MessageEvent event) {
        if (nickChanges.isEmpty()) {
            event.getBot().sendNotice(event.getUser(), "No Nick Changes found.");
        }
        String users = "";
        for (Map.Entry<String, String> curUser : nickChanges.entrySet()) {
            //Add original nick
            users += curUser.getKey();
            //Add middle text
            users += " changed to ";
            //Add new nick
            users += curUser.getValue();
            //Add seperator
            users += ", ";
        }
        //Remove last separator
        users = users.substring(0, users.length() - 3);

        //Send to user in a PM
        event.getBot().sendNotice(event.getUser(), "Nick Changes: " + users);
    }

    @Command(help = "Clear Nick Changes", usage = "clearnickchanges")
    public static void clearnickchanges(MessageEvent event) {
        nickChanges.clear();
        event.getBot().sendNotice(event.getUser(), "Nick Changes Cleared.");
    }

    @Command(help = "Prints the time", usage = "time", maxArgs = 0, minArgs = 0)
    public static void time(MessageEvent event) {
        event.getBot().sendNotice(event.getUser(), new Date().toString());
    }

    @EventListener(name = "ShortURLResolver", event = "MessageEvent")
    public static void shortUrlResolver(MessageEvent event) throws IOException, ParserConfigurationException, SAXException {
        String message = event.getMessage();
        String lastparse = "";
        if (message.contains("http://goo.gl/")) {
            String googleurllink = "";
            for (String url : pullLinks(message)) {
                if (url.startsWith("http://goo.gl/") || url.length()==19) {
                    googleurllink = url;
                    break;
                }
            }
            if (googleurllink.equals("")) {
                return;
            }
            String redirected = getLocationRedirect(googleurllink);
            lastparse = redirected;
            if (redirected.equals(googleurllink)) {
                event.getBot().sendNotice(event.getUser(), "Goo.Gl Failed!");
            }
            event.getBot().sendNotice(event.getUser(), "URL: " + googleurllink);
            event.getChannel().sendMessage("$> " + googleurllink + " > " + redirected);
        }
        if (message.contains("http://bit.ly/")) {
            String googleurllink = "";
            for (String url : pullLinks(message)) {
                if (url.startsWith("http://bit.ly/")) {
                    if (url.equals("http://bit.ly"))
                        continue;
                    if (url.equals("http://bit.ly"))
                        continue;
                    googleurllink = url;
                    break;
                }
            }
            String redirected = getLocationRedirect(googleurllink);
            if (redirected.equals(googleurllink)) {
                event.getBot().sendNotice(event.getUser(), "Bit.Ly Failed!");
            }
            lastparse = redirected;
            event.getChannel().sendMessage("$> " + googleurllink + " > " + redirected);
        }
        // Make Sure youtube url resolver is at the bottom to ensure if short url directs to youtube that its title is printed.
        if (message.contains("youtube.com/")) {
            String youtubeURL = "";
            for (String url : pullLinks(message)) {
                if (url.contains("youtube.com/watch")) {
                    youtubeURL = url;
                    break;
                }
            }
            String youtubeVID = youtubeURL.replace("youtube.com/watch?v=", " ").replace('&', ' ').split(" ")[1];
            String title = getYouTubeTitle(youtubeVID);
            event.getChannel().sendMessage("$> " + title);
            System.out.println("YouTubeURL2Title: Video Title for " + youtubeVID + ": " + title);
        }
        if (message.contains("http://youtu.be/")) {
            String youtubeURL = "";
            for (String url : pullLinks(message)) {
                if (url.contains("http://youtu.be/watch")) {
                    youtubeURL = url;
                    break;
                }
            }
            String youtubeVID = youtubeURL.replace("http://youtu.be/", " ");
            String title = getYouTubeTitle(youtubeVID);
            event.getChannel().sendMessage("$> " + title);
            System.out.println("YouTubeURL2Title: Video Title for " + youtubeVID + ": " + title);
        }
        if (lastparse.contains("youtube.com/")) {
            String youtubeURL = "";
            for (String url : pullLinks(lastparse)) {
                if (url.contains(".youtube.com/watch")) {
                    youtubeURL = url;
                    break;
                } else if (url.contains("http://youtube.com/watch")) {
                    youtubeURL = url;
                    break;
                }
            }
            String youtubeVID = youtubeURL.replace("youtube.com/watch?v=", " ").replace('&', ' ').split(" ")[1];
            String title = getYouTubeTitle(youtubeVID);
            event.getChannel().sendMessage("$> " + title);
            System.out.println("YouTubeURL2Title: Video Title for " + youtubeVID + ": " + title);
        }
    }

    @Command(help = "Message ChanServ with arguments", usage = "chanserv <MESSAGE>", minArgs = 1, maxArgs = -1)
    public static void chanserv(MessageEvent event, String[] args) {
        event.getBot().sendMessage("ChanServ", argsAsString(args));
    }

    @EventListener(event = "MessageEvent", name = "JiraIssueURLName")
    public static void jiraName(MessageEvent event) throws IOException {
        ArrayList<String> links = pullLinks(event.getMessage());
        String link = "";
        for (int i = 0; i<links.size(); i++) {
            if (links.get(i).contains("/browse/")) {
                link = links.get(i);
            } else if (i + 1==links.size()){
                break;
            }
        }
        if (link.equals("")) {
            return;
        }
        link = link.replace("/browse/", "/rest/api/latest/issue/");
        Gson gson = new Gson();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(link).openConnection();
        BASE64Encoder enc = new sun.misc.BASE64Encoder();
        String userpassword = "admin" + ":" + "152161alex";
        String encodedAuthorization = enc.encode(userpassword.getBytes());
        httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
        httpURLConnection.connect();
        if (httpURLConnection.getResponseCode()!=200) {
            if (httpURLConnection.getResponseCode()==404) {
                event.getChannel().sendMessage("$> Invalid Issue!");
            } else {
                event.getChannel().sendMessage("$> Error: Server Returned " + httpURLConnection.getResponseCode() + " (" + httpURLConnection.getResponseMessage());
            }
            return;
        }
        JiraAPI.JiraIssue data = gson.fromJson(new InputStreamReader(httpURLConnection.getInputStream()), JiraAPI.JiraIssue.class);
        String message2send = "$> " + data.getKey() + ": " + data.getFields().getSummary();
        event.getChannel().sendMessage(message2send);
        httpURLConnection.disconnect();
    }

    @Command(help = "Get Info on JIRA Issues", usage = "jira <KEY>", maxArgs = 1, minArgs = 1)
    @SuppressWarnings("literal")
    public static void jira(MessageEvent event, String[] args) throws IOException {
        String key = argsAsString(args);
        String url = AllThemUtilities.jiraURL + "rest/api/latest/issue/";
        String urls = url + key;
        Gson gson = new Gson();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(urls).openConnection();
        BASE64Encoder enc = new sun.misc.BASE64Encoder();
        String userPassword = AllThemUtilities.jiraAuth;
        String encodedAuthorization = enc.encode(userPassword.getBytes());
        httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
        try {
            httpURLConnection.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (httpURLConnection.getResponseCode()!=200) {
            if (httpURLConnection.getResponseCode()==404) {
                event.getChannel().sendMessage("$> Invalid Issue!");
            } else {
                event.getChannel().sendMessage("$> Error: Server Returned " + httpURLConnection.getResponseCode() + " (" + httpURLConnection.getResponseMessage() + ")");
            }
            return;
        }
        JiraAPI.JiraIssue data = gson.fromJson(new InputStreamReader(httpURLConnection.getInputStream()), JiraAPI.JiraIssue.class);
        String message2send = "$> " + data.getKey() + ": "  + data.getFields().getSummary() + " by " + data.getFields().getReporter().getDisplayName() + " (Status: " + data.getFields().getStatus().getName() + ")";
        event.getChannel().sendMessage(message2send);
        httpURLConnection.disconnect();
    }

    public static String argsAsString(String[] args) {
        return StringUtils.arrayAsString(args);
    }

    public static String argsAsString(String[] args, int start) {
        return StringUtils.arrayAsString(args, start);
    }

    public static ArrayList<String> pullLinks(String text) {
        ArrayList<String> links = new ArrayList<String>();
        String regex = "\\(?\\b((http|https)://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while(m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")"))
            {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }
        return links;
    }

    public static String getLocationRedirect(String urlString) throws IOException {
        HttpURLConnection connection = HTTPUtils.getConnection(urlString);
        if (connection==null) {
            return "";
        }
        System.out.println("Resolving URL: " + urlString);
        connection.setInstanceFollowRedirects(true);
        try {
            connection.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String redirected = connection.getHeaderField("Location");
        connection.disconnect();
        try {
            return URLDecoder.decode(redirected, "UTF-8");
        } catch (Exception e) {
            return redirected;
        }
    }

    public static String getYouTubeTitle(String videoId) throws ParserConfigurationException, IOException, SAXException {
        String vidInfoURL = "http://gdata.youtube.com/feeds/api/videos/" + videoId + "?v=2";
        HttpURLConnection connection = (HttpURLConnection) new URL(vidInfoURL).openConnection();
        connection.connect();
        if (connection.getResponseCode()!=200) {
            return "Error: Video does not exist or it is not public.";
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(connection.getInputStream());
        NodeList nList = doc.getElementsByTagName("entry");
        Element eElement = (Element) nList.item(0);
        String title = eElement.getElementsByTagName("title").item(0).getTextContent();
        // I wish somebody could explain why this is the title for non-existant videos...
        if (title.equals("MACKLEMORE & RYAN LEWIS - THRIFT SHOP FEAT. WANZ (OFFICIAL VIDEO)")) {
            title = "Error: Video does not exist.";
        }
        connection.disconnect();
        return title;
    }

    @Command(help = "Get a random user", usage = "randomUser", maxArgs = 0, minArgs = 0)
    public static void randomUser(MessageEvent event, String[] args) {
        User user = RandomEntities.getRandomUser();
        if (user==null) {
            event.getBot().sendNotice(event.getUser(), "Error: Random User was null!");
        } else {
            event.getBot().sendNotice(event.getUser(), "User: " + user.getNick());
        }
    }

    @Command(help = "Change Bot Nickname", usage = "botnick <NICKNAME>", maxArgs = 1, minArgs = 1)
    public static void botnick(MessageEvent event, String[] args) {
        event.getBot().changeNick(args[0]);
    }
}