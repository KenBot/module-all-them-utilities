package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.registry.EventListener;
import kaendfinger.kenbot.api.utils.StringUtils;
import org.pircbotx.hooks.events.MessageEvent;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class PrivateCmds {
    private static Properties prop = new Properties();

    public static void addTextCommand(String cmd, String text4cmd) {
        prop.setProperty(cmd, text4cmd);
        storeProps();
    }

    public static boolean hasCmdPerms(String user, String command) {
        if (prop.containsKey(command + ".perm")) {
            String[] users = prop.getProperty(command + ".perm").trim().split(" ");
            for (String curUser : users) {
                if (curUser.equals(user)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public static String getTextForCommand(String cmd) {
            if (!(prop.containsKey(cmd))) {
                return null;
            } else {
                return prop.getProperty(cmd);
            }
    }

    @EventListener(event = "MessageEvent", name = "PrivateTextCommandHandler")
    public static void commandHandler(MessageEvent event) {
        String[] fullCmd = event.getMessage().split(" ");
        String text = getTextForCommand(fullCmd[0]);
        String user = event.getUser().getNick();
        if (fullCmd[0].endsWith(".perm")) {
            return;
        }
        if (text != null) {
            if (hasCmdPerms(user, fullCmd[0])) {
                event.getBot().sendNotice(event.getUser(), "$>" + text);
            } else {
                event.getBot().sendNotice(event.getUser(), "Error: You do not have permissions to use this command.");
            }
        }
    }

    @Command(help = "Adds a Private Text Command", usage = "addprivatecmd <command> <text>", minArgs = 2, maxArgs = -1)
    public static void addprivatecmd(MessageEvent event, String[] args) {
        loadProps();
        if (args[0].endsWith(".perm")) {
            event.getBot().sendNotice(event.getUser(), "ERROR: You cannot add a command ending in \".perm\"");
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            builder.append(" ");
            builder.append(args[i]);
        }
        if (!prop.containsKey(args[0] + ".perm")) {
            prop.setProperty(args[0] + ".perm", event.getUser().getNick());
        }
        addTextCommand(args[0], builder.toString());
        event.getBot().sendNotice(event.getUser(), "Successfully added the " + args[0] + " command");
    }

    @Command(help = "Removes a Private Text Command", usage = "removeprivatecmd <command>", minArgs = 1, maxArgs = 1)
    public static void removeprivatecmd(MessageEvent event, String[] args) {
        if (!(prop.containsKey(args[0]))) {
            event.getBot().sendNotice(event.getUser(), "ERROR: Command " + args[0] + " does not exist!");
            return;
        }
        if (args[0].endsWith(".perm")) {
            event.getBot().sendNotice(event.getUser(), "ERROR: Command " + args[0] + " does not exist!");
            return;
        }
        prop.remove(args[0]);
        event.getBot().sendNotice(event.getUser(), "Successfully removed the " + args[0] + " command.");
        storeProps();
    }

    @Command(help = "Set Permissions on Private Text Commands", usage = "setprivcmdperm <command> <users>" , minArgs = 1, maxArgs = -1)
    public static void setprivcmdperm(MessageEvent event, String[] args) {
        String command = args[0];
        if (!prop.containsKey(command)) {
            event.getBot().sendNotice(event.getUser(), "ERROR: Command " + command + " does not exist!");
            return;
        }
        if (args.length==1) {
            prop.remove(command + ".perm");
            event.getBot().sendNotice(event.getUser(), "All Permissions Removed for command " + command);
        } else {
            String users = StringUtils.getArgsAsString(args, 1);
            prop.setProperty(command + ".perm", users);
            event.getBot().sendNotice(event.getUser(), "Permissions Set for command " + command);
            storeProps();
        }
    }

    @Command(help = "Lists Private Text Commands", usage = "listprivatecmds", minArgs = 0, maxArgs = 0)
    public static void listprivatecmds(MessageEvent event) {
        StringBuilder sb = new StringBuilder();
        Set<Object> cmds = prop.keySet();
        Iterator<Object> itr = cmds.iterator();
        while (itr.hasNext()) {
            String next = (String) itr.next();
            if (next.endsWith(".perm")) {
                continue;
            }
            if (itr.hasNext()) {
                sb.append(next);
                sb.append(" ");
            } else {
                sb.append(next);
            }
        }
        event.getBot().sendNotice(event.getUser(), "Private Text Commands: " + sb.toString());
    }

    @Command(help = "Reloads Private Text Commands", usage = "reloadprivatecmds", minArgs = 0, maxArgs = 0)
    public static void reloadprivatecmds(MessageEvent event, String[] args) {
        loadProps();
        event.getBot().sendNotice(event.getUser(), "Successfully reloaded private text commands!");
    }

    public static void loadProps() {
        try {
            prop.load(new FileInputStream("data/text-commands.private.db"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void storeProps() {
        try {
            prop.store(new FileOutputStream("data/text-commands.private.db"), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
