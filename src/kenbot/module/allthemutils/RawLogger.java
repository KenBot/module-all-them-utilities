package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.EventListener;
import org.pircbotx.hooks.events.RawEvent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class RawLogger {
    private static BufferedWriter writer;
    @EventListener(event = "RawEvent", name = "RawEventLogger")
    public static void logRawEvent(RawEvent event) {
        log(event.getMessage());
    }

    public static void init(File par1File) {
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(par1File)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void log(String line) {
        if (writer==null) {
            return;
        }
        try {
            writer.write(line);
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
