package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.EventListener;
import org.pircbotx.hooks.events.RawEvent;

@SuppressWarnings("UnusedDeclaration")
public class RawServiceHandler {
    @EventListener(name = "ServicesParser", event = "RawEvent")
    public static void serviceHandler(RawEvent event) {
        String[] rawLine = event.getMessage().split(" ");
        if (!event.getBot().userExists(AllThemUtilities.owner)) {
            return;
        }
        for (String service : AllThemUtilities.servicesToHandle) {
            if (rawLine[0].startsWith(":" + service + "!")) {
                if (rawLine[1].equals("NOTICE")) {
                    String message = OtherCommands.argsAsString(rawLine, 3);
                    String newMessage = message.replace(":", "");
                    event.getBot().sendNotice(AllThemUtilities.owner, service + ": " + newMessage);
                }
            }
        }
    }
}