package kenbot.module.allthemutils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class TextCmdsLog {
    private static File logFile = new File("log/textcommands.log");
    private static BufferedWriter out;
    public static void load() {
        if (!AllThemUtilities.allowLogging) {
            return;
        }
        try {
            if (!logFile.exists()) {
                if (!logFile.createNewFile()) {
                    System.out.println("Error! Cannot create log! Setting Allow Logging to false.");
                    AllThemUtilities.allowLogging = false;
                }
            }
            out = new BufferedWriter(new FileWriter(logFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void append(String line) {
        if (!AllThemUtilities.allowLogging) {
            return;
        }
        try {
            out.write(line);
            out.newLine();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}