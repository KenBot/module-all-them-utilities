package kenbot.module.allthemutils;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.registry.EventListener;
import kaendfinger.kenbot.api.utils.StringUtils;
import kaendfinger.kenbot.core.KenBotConfig;
import kaendfinger.kenbot.listeners.CommandListener;
import org.pircbotx.hooks.events.MessageEvent;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class TextCommands {
    private static Properties prop = new Properties();

    public static void addTextCommand(String cmd, String text4cmd) {
        prop.setProperty(cmd, text4cmd);
        storeProps();
    }

    public static String getTextForCommand(String cmd) {
        if (!(prop.containsKey(cmd))) {
            return null;
        } else {
            return prop.getProperty(cmd);
        }
    }

    @EventListener(event = "MessageEvent", name = "TextCommandHandler")
    public static void commandHandler(MessageEvent event) {
        String[] fullCmd = event.getMessage().split(" ");
        String text = getTextForCommand(fullCmd[0]);
        if (text != null) {
            if (CommandListener.commandExists(fullCmd[0].replaceAll(KenBotConfig.get("prefixChar"), ""))) {
                return;
            }
            StringBuilder message = new StringBuilder();
            if (fullCmd.length>1) {
                String userToPing = fullCmd[1];
                if (event.getBot().userExists(userToPing)) {
                    message.append(userToPing);
                    message.append(":");
                    message.append(text);
                } else {
                    message.append("$>");
                    message.append(text);
                }
            } else {
                message.append("$> ");
                message.append(text);
            }
            event.getChannel().sendMessage(message.toString());
        }
    }

    @Command(help = "Adds a Text Command", usage = "addtxtcmd <command> <text>", minArgs = 2, maxArgs = -1)
    public static void addtxtcmd(MessageEvent event, String[] args) {
        loadProps();
        StringUtils.arrayAsString(args, 1);
        addTextCommand(args[0], StringUtils.arrayAsString(args, 1));
        event.getBot().sendNotice(event.getUser(), "Successfully added the " + args[0] + " command.");
    }

    @Command(help = "Reloads Text Commands", usage = "reloadtxtcmds", minArgs = 0, maxArgs = 0)
    public static void reloadtxtcmds(MessageEvent event, String[] args) {
        loadProps();
        event.getBot().sendNotice(event.getUser(), "Successfully reloaded text commands!");
    }

    @Command(help = "Removes a Text Command", usage = "removetxtcmd <command>", minArgs = 1, maxArgs = 1)
    public static void removetxtcmd(MessageEvent event, String[] args) {
        if (!(prop.containsKey(args[0]))) {
            event.getBot().sendNotice(event.getUser(), "ERROR: Command " + args[0] + " does not exist!");
            return;
        }
        prop.remove(event.getMessage().split(" ")[1]);
        event.getBot().sendNotice(event.getUser(), "Successfully removed the " + args[0] + " command.");
        storeProps();
    }

    @Command(help = "Lists Text Commands", usage = "listtxtcmds", minArgs = 0, maxArgs = 0)
    public static void listtxtcmds(MessageEvent event) {
        StringBuilder sb = new StringBuilder();
        Set<Object> cmds = prop.keySet();
        Iterator<Object> itr = cmds.iterator();
        while (itr.hasNext()) {
            if (itr.hasNext()) {
                sb.append((String) itr.next());
                sb.append(" ");
            } else {
                sb.append((String) itr.next());
            }
        }
        event.getBot().sendNotice(event.getUser(), "Text Commands: " + sb.toString());
    }

    public static void loadProps() {
        try {
            prop.load(new FileInputStream("data/text-commands.db"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void storeProps() {
        try {
            prop.store(new FileOutputStream("data/text-commands.db"), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Command(help = "Manage Text Commands", usage = "textcmds SUB-COMMAND", maxArgs = -1, minArgs = 1)
    public static void textcmds(MessageEvent event, String[] args) {
        String command = args[0];
        String commands = "add, remove, list";
        switch (command) {
            case "add":
                if (args.length<3) {
                    event.getBot().sendNotice(event.getUser(), "Usage: textcmds add COMMAND MESSAGE");
                    return;
                }
                loadProps();
                StringUtils.arrayAsString(args, 1);
                addTextCommand(args[1], StringUtils.arrayAsString(args, 2));
                event.getBot().sendNotice(event.getUser(), "Successfully added the " + args[1] + " command.");
                return;
            case "remove":
                if (args.length<2) {
                    event.getBot().sendNotice(event.getUser(), "Usage: textcmds remove TEXT-COMMAND");
                    return;
                }
                if (!(prop.containsKey(args[1]))) {
                    event.getBot().sendNotice(event.getUser(), "ERROR: Command " + args[1] + " does not exist!");
                    return;
                }
                prop.remove(args[1]);
                event.getBot().sendNotice(event.getUser(), "Successfully removed the " + args[1] + " command.");
                storeProps();
                return;
            case "list":
                StringBuilder sb = new StringBuilder();
                Set<Object> cmds = prop.keySet();
                Iterator<Object> itr = cmds.iterator();
                while (itr.hasNext()) {
                    if (itr.hasNext()) {
                        sb.append((String) itr.next());
                        sb.append(" ");
                    } else {
                        sb.append((String) itr.next());
                    }
                }
                event.getBot().sendNotice(event.getUser(), "Text Commands: " + sb.toString());
                return;
            case "reload":
                loadProps();
                event.getBot().sendNotice(event.getUser(), "Successfully reloaded text commands!");
                return;
            default:
                event.getBot().sendNotice(event.getUser(), "Invalid Command! Possible Commands: " + commands);
        }

    }
}
