package kenbot.module.allthemutils.api;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class HTTPUtils {
    public static HttpURLConnection getConnection(String par1) {
        URL url = null;
        try {
            url = new URL(par1);
        } catch (MalformedURLException e) {
            doError(e);
            return null;
        }
        try {
            return (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            doError(e);
            return null;
        }
    }

    private static void doError(Exception e) {
        System.out.println("Error in HTTPUtils: " + e.getLocalizedMessage());
        System.out.print("Saving Stack Trace....");
        Random random = new Random();
        File stackFile = new File("error.HTTPUtils." + random.nextInt(2000));
        try {
            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(stackFile)));
            e.printStackTrace(out);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        System.out.println("Error Saved to " + stackFile.getName());
    }
}
