package kenbot.module.allthemutils.core;

import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.core.BotExtender;
import org.pircbotx.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class RandomEntities {
    public static String[] ignore = new String[] {
            "NickServ",
            "ChanServ",
            "MemoServ",
            "OpServ",
            "QuakeBot"
    };
    public static User getRandomUser() {
        BotExtender bot = BotUtils.getBot();
        Set<User> usersSet = bot.getUsers();
        Random random = new Random();
        User[] users = usersSet.toArray(new User[]{});
        int userNumber = random.nextInt(users.length);
        return users[userNumber];
    }

}
